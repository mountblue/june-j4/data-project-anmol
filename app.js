const express = require("express");
const path = require("path");
const matchData = path.resolve("assets/data-files/ipl/matches.csv");
const deliveryData = "assets/data-files/ipl/deliveries.csv";
const operations = require(path.resolve("fun.js"));
let app = express();
const ejs = require('ejs');

app.set('view engine', 'ejs');
app.use(express.static("public"));

operations.getMatchesPerYear(matchData).then(function (data) {
    app.get('/chart1', function (req, res) {
        res.render('hc1', { data: JSON.stringify(data) });
    });

})

operations.matchDetails(matchData, 2016).then(function (data) {
    operations.getExtraRunsPerTeam(data, deliveryData).then(function (data) {
        app.get('/chart2', function (req, res) {
            res.render('hc2', { data: JSON.stringify(data) });
        });


    })
})

operations.matchDetails(matchData, 2015).then(function (data) {
    ;
    operations.getTopEconomicalBowler(data, deliveryData).then(function (data) {
        app.get('/chart3', function (req, res) {
            res.render('hc3', { data: JSON.stringify(data) });
        });


    })
})


operations.matchDetails(matchData, 2016).then(function (data) {
    operations.getTopBatsman(data, deliveryData).then(function (data) {

        app.get('/chart4', function (req, res) {
            res.render('hc4', { data: JSON.stringify(data) });
        });
    })
})


operations.getMatchesWonAllYears(matchData).then(function (data) {
    app.get('/chart5', function (req, res) {
        res.render('hc5', { data: JSON.stringify(data) });
    });
})

app.listen(8000);
console.log("listening");