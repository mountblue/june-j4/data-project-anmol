const fs = require('fs');
const path = require('path');
const csv = require('fast-csv');
const matchData = "assets/data-files/ipl/matches.csv"
const deliveryData = "assets/data-files/ipl/deliveries.csv";

// first chart
let getMatchesPerYear = function (matchData) {

    return new Promise(function (resolve, reject) {
        let matchesPerSeason = {};
        fs.readFile(matchData, function (err, data) {
            if (err) {
                reject(err);
            } else {
                data.toString().split("\n").forEach(function (line, index, arr) {
                    if (index !== 0) {
                        const match = line.split(",");
                        const season = match[1];
                        if (matchesPerSeason.hasOwnProperty(season)) {
                            matchesPerSeason[season]++;
                        } else {
                            matchesPerSeason[season] = 1;
                        }
                    }
                })
            }
            resolve(matchesPerSeason);
        })
    })

}

//second chart
let matchDetails = function (matchData, year) {
    return new Promise(function (resolve, reject) {
        let matchArr = [];
        let csvStream = csv.fromPath(matchData, { headers: true })
            .on("data", function (data) {
                csvStream.pause();
                let id = data.id;
                let season = data.season;

                if (parseInt(season) === year) {
                    matchArr.push(parseInt(id));
                }
                csvStream.resume();
            }).on("end", function () {
                resolve(matchArr);

            }).on("error", function (err) {
                console.log(err);
            });

    });
}

let getExtraRunsPerTeam = function (arrData, deliveryData) {

    return new Promise(function (resolve, reject) {
        let extraRunsArr = {};
        let csvStream = csv.fromPath(deliveryData, { headers: true })
            .on("data", function (data) {
                csvStream.pause();
                let id = data.match_id;
                let teamName = data.bowling_team;
                let extraRuns = parseInt(data.extra_runs);

                if (arrData.includes(parseInt(id))) {
                    if (extraRunsArr.hasOwnProperty(teamName)) {
                        extraRunsArr[teamName] = extraRunsArr[teamName] + extraRuns;
                    } else {
                        extraRunsArr[teamName] = extraRuns;
                    }
                }

                csvStream.resume();
            }).on("end", function () {
                resolve(extraRunsArr);

            }).on("error", function (err) {
                console.log(err);
            });

    });

}

//third chart

let getTopEconomicalBowler = function (arrData, deliveryData) {
    return new Promise(function (resolve, reject) {
        let topBowler = {};
        let bowlers = {};
        let runsByBowler = {};
        let sortedList = {};
        let sortByEconomy = {};
        let csvStream = csv.fromPath(deliveryData, { headers: true })
            .on("data", function (data) {
                csvStream.pause();
                let id = parseInt(data.match_id)
                let totalRuns = parseInt(data.total_runs);
                let ball = parseInt(data.ball);
                let playerName = data.bowler;
                if (arrData.includes(id)) {
                    if (bowlers.hasOwnProperty(playerName)) {
                        runsByBowler[playerName] = runsByBowler[playerName] + totalRuns;
                        bowlers[playerName] = bowlers[playerName] + ball;
                    } else {
                        bowlers[playerName] = ball;
                        runsByBowler[playerName] = totalRuns;
                    }
                }
                csvStream.resume();
            }).on("end", function () {
                for (let key in bowlers) {
                    topBowler[key] = parseFloat((runsByBowler[key] / bowlers[key] * 6).toFixed(2));
                }
                sortByEconomy = Object.keys(topBowler).sort(function (a, b) { return topBowler[a] - topBowler[b] });
                sortByEconomy.map(function (key, index) {
                    sortedList[key] = topBowler[key];
                })
                resolve(sortedList);
            }).on("error", function (err) {
                console.log(err);
            });

    });
}

//fourth chart

let getTopBatsman = function (arrData, deliveryData) {
    return new Promise(function (resolve, reject) {
        let topBatsman = {};
        let sortedList = {};
        let sortByRuns = {};
        let csvStream = csv.fromPath(deliveryData, { headers: true })
            .on("data", function (data) {
                csvStream.pause();
                let id = data.match_id;
                let batsmanName = data.batsman;
                let runs = parseInt(data.batsman_runs);

                if (arrData.includes(parseInt(id))) {
                    if (topBatsman.hasOwnProperty(batsmanName)) {
                        topBatsman[batsmanName] = topBatsman[batsmanName] + runs;
                    } else {
                        topBatsman[batsmanName] = runs;
                    }
                }

                csvStream.resume();
            }).on("end", function () {
                sortByRuns = Object.keys(topBatsman).sort(function (a, b) { return topBatsman[b] - topBatsman[a] });
                sortByRuns.map(function (key, index) {
                    sortedList[key] = topBatsman[key];
                })
                resolve(sortedList);
            }).on("error", function (err) {
                console.log(err);
            });

    });
}

let getMatchesWonAllYears = function (matchesFile) {
    return new Promise(function (resolve, reject) {
        let matchesWon = {};
        let count = 0;
        let csvStream = csv.fromPath(matchesFile, { headers: true })
            .on("data", function (match) {
                let season = match.season;
                let winner = match.winner;
                if (count) {
                    if (winner) {
                        if (!matchesWon[season]) {
                            matchesWon[season] = {};
                        }
                        if (!matchesWon[season][winner]) {
                            matchesWon[season][winner] = 1;
                        } else {
                            matchesWon[season][winner]++;
                        }
                    }
                }
                count++;
            })
            .on("end", function () {
                resolve(matchesWon);
            })
    })

}
module.exports = {
    getMatchesPerYear,
    getExtraRunsPerTeam,
    matchDetails,
    getTopEconomicalBowler,
    getTopBatsman,
    getMatchesWonAllYears
}