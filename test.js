const expect = require('chai').expect;
const path = require('path');
const matchData = path.resolve("assets/data-files/ipl/demo-matches.csv");
const deliveryData = path.resolve("assets/data-files/ipl/demo-deliveries.csv");
const fileName = path.resolve("fun.js");
const operations = require(fileName);


describe("the number of matches played per year of all the years in IPL.", function () {
    it("should exist", function () {
        expect(operations.getMatchesPerYear).to.exist;
    })

    it("should not return null", function () {
        expect(operations.getMatchesPerYear).to.be.not.null;
    })

    it("should return an array object", function (done) {
        operations.getMatchesPerYear(matchData).then(function (data) {
            try {
                expect(data).to.be.an('object');
                done();
            } catch (e) {
                done(e);
            }

        })
    })

    it("should read the file", function (done) {
        operations.getMatchesPerYear(matchData).then(function (data) {
            try {
                expect(operations.getMatchesPerYear).to.not.throw(Error);
                done();
            } catch (e) {
                done(e);
            }

        })
    })

    it("should be the expected result", function (done) {

        let expectedResult = {
            2017: 2,
            2009: 3,
            2016: 1,
            2015: 2
        }

        operations.getMatchesPerYear(matchData).then(function (data) {
            try {
                expect(data).to.deep.equal(expectedResult)
                done();
            } catch (e) {
                done(e);
            }

        })

    })

})

//

describe("For the year 2016 plot the extra runs conceded per team.", function () {
    it("should exist", function () {
        expect(operations.getExtraRunsPerTeam).to.exist;
    })

    it("should not return null", function () {
        expect(operations.getExtraRunsPerTeam).to.be.not.null;
    })


    it("should read the file", function (done) {

        operations.matchDetails(matchData, 2016).then(function (data) {
            operations.getExtraRunsPerTeam(data, deliveryData).then(function (data) {

                try {
                    done();
                } catch (e) {
                    done(e);
                }

            })
        })
    })

    it("should be the expected result", function (done) {
        let expectedResult = {
            'Gujarat Lions': 1
        }
        operations.matchDetails(matchData, 2016).then(function (data) {
            operations.getExtraRunsPerTeam(data, deliveryData).then(function (data) {

                try {
                    expect(data).to.deep.equal(expectedResult)
                    done();
                } catch (e) {
                    done(e);
                }

            })
        })
    })
})

//

describe("For the year 2015 plot the top economical bowlers.", function () {
    it("should exist", function () {
        expect(operations.getTopEconomicalBowler).to.exist;
    })

    it("should not return null", function () {
        expect(operations.getTopEconomicalBowler).to.be.not.null;
    })


    it("should read the file", function (done) {

        operations.matchDetails(matchData, 2015).then(function (data) {

            operations.getTopEconomicalBowler(data, deliveryData).then(function (data) {

                try {
                    done();
                } catch (e) {
                    done(e);
                }

            })
        })
    })

    it("should be the expected result", function (done) {
        const expectedResult = {
            'R Vinay Kumar': 15,
            'Z Khan': 3
        }
        operations.matchDetails(matchData).then(function (data) {
            console.log(data);
            operations.getTopEconomicalBowler(data, deliveryData).then(function (data) {

                try {
                    expect(data).to.deep.equal(expectedResult)
                    done();
                } catch (e) {
                    done(e);
                }

            })

        })
    })
})

//

describe("For the year 2016 plot the top batsman with highest.", function () {
    it("should exist", function () {
        expect(operations.getTopBatsman).to.exist;
    })

    it("should not return null", function () {
        expect(operations.getTopBatsman).to.be.not.null;
    })


    it("should read the file", async function () {

        let matchResult = await operations.matchDetails(matchData, 2016)
        let deliveriesResult = await operations.getTopBatsman(matchResult, deliveryData)

        
    })

    it("should be the expected result", function (done) {
        const expectedResult = {
            'M Vijay': 1
        }
        operations.matchDetails(matchData, 2016).then(function (data) {

            operations.getTopBatsman(data, deliveryData).then(function (data) {

                try {
                    expect(data).to.deep.equal(expectedResult)
                    done();
                } catch (e) {
                    done(e);
                }

            })

        })
    })
})

//

describe("matches won of all teams over all the years of IPL.", function () {
    it("should exist", function () {
        expect(operations.getMatchesWonAllYears).to.exist;
    })

    it("should not return null", function () {
        expect(operations.getMatchesWonAllYears).to.be.not.null;
    })


    it("should read the file", function (done) {

        operations.matchDetails(matchData, 2016).then(function (data) {
            console.log(data);
            operations.getMatchesWonAllYears(data, deliveryData).then(function (data) {

                try {
                    done();
                } catch (e) {
                    done(e);
                }

            })
        })
    })
})

