let years = [];
let values = [];
let allTeam = [];
for (var key in matchData) {
    years.push(key)
    for (nameOfTeam in matchData[key]) {
        if (!allTeam.includes(nameOfTeam)) {
            allTeam.push(nameOfTeam);
        }
    }
}
let mainArr = [];
for (let j in allTeam) {
    let arr = [];
    for (let i in years) {
        if (matchData[years[i]].hasOwnProperty(allTeam[j])) {
            arr.push(matchData[years[i]][allTeam[j]]);
        }
        else {
            arr.push(0);
        }
    }
    let obj = {
        'name': allTeam[j],
        'data': arr
    }
    mainArr.push(obj);
}


Highcharts.chart('high-chart', {
    chart: {
        type: 'bar'
    },
    title: {
        text: 'Stacked bar chart'
    },
    xAxis: {
        categories: years
    },
    yAxis: {
        min: 0,
        title: {
            text: 'Matches won of all teams over all the years of IPL.'
        }
    },
    legend: {
        reversed: true
    },
    plotOptions: {
        series: {
            stacking: 'normal'
        }
    },
    series: mainArr
});